import RPi.GPIO as GPIO
import os
import atexit
import time

# GPIO PIN DEFINES

pin_sr_clk =  4
pin_sr_noe = 17
pin_sr_dat = 27 # NOTE: if you have a RPi rev.1, need to change this to 21
pin_sr_lat = 22

# NUMBER OF STATIONS
num_stations = 12

# STATION BITS
num_stations = 12

station_bits  = [0]*num_stations
# watering_time = [12,0,6,8,6,8,5,8,8,8,8,5]
watering_time = [10,0,6,8,6,8,5,8,8,8,8,5]


def enableShiftRegisterOutput():
    GPIO.output(pin_sr_noe, False)

def disableShiftRegisterOutput():
    GPIO.output(pin_sr_noe, True)

def setShiftRegister(values):
    GPIO.output(pin_sr_clk, False)
    GPIO.output(pin_sr_lat, False)
    for s in range(0,num_stations):
        GPIO.output(pin_sr_clk, False)
        GPIO.output(pin_sr_dat, values[num_stations-1-s])
        GPIO.output(pin_sr_clk, True)
    GPIO.output(pin_sr_lat, True)
    
def water_lawn(station_bits, watering_time):
    for station in range(num_stations):
        print "Watering station %d" % station
        # Turn on that station
        station_bits[station] = 1
        setShiftRegister(station_bits)
        time.sleep(watering_time[station] * 60)
        print "Finished watering station %d" % station
        # Turn off that station
        station_bits[station] = 0
        setShiftRegister(station_bits)
    
def run():
    GPIO.cleanup()
    # setup GPIO pins to interface with shift register
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin_sr_clk, GPIO.OUT)
    GPIO.setup(pin_sr_noe, GPIO.OUT)
    disableShiftRegisterOutput()
    GPIO.setup(pin_sr_dat, GPIO.OUT)
    GPIO.setup(pin_sr_lat, GPIO.OUT)

    setShiftRegister(station_bits)
    enableShiftRegisterOutput()
    
    water_lawn(station_bits, watering_time)
    exit()
    
def progexit():
    # Reset everyting to zeros
    global station_bits
    station_bits = [0]*num_stations
    setShiftRegister(station_bits)
    GPIO.cleanup()

if __name__ == '__main__':
    atexit.register(progexit)
    run()